# <h3>Python einstieg with GitLab</h3>


1. GitLab özelliklerini tanıma anlama ve kullanma
2. [Kodierun mit Python und Commit zu meinem GitLab Repository](https://gitlab.com/caneru/python-einstieg-with-gitlab/-/edit/master/README.md)

|NU| AUFGABE 1 | AUFGABE 2 |
|--| ------ | ------ |
|1| gib aus **Hallo Welt** | Ping&Pong |
|2| grüss nur **Aline&Fabian** | cell |
|3| gib Name&Land ein | cell |
|4| grüss mit eigene Sprache | cell |
|5| cell | cell |

